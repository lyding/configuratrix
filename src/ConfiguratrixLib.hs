{-# LANGUAGE MultiWayIf #-}

module ConfiguratrixLib 
    ( test,
      Config,
      skipChar,
      parseInteger,
      parseDouble,
      parseList,
      parseName,
      genReadConfig,
      genListParser,
      liftParser,
      splitToCmds,
      createArrayParser,
      createParserFromRead,
      getValueByName,
      isErroneous,
      getConfig,
      getErrors
    ) where

import System.IO
import Data.Either
import Data.List.Split (splitOneOf)
import Data.Attoparsec.Text hiding (I, D)
import qualified Data.Text as T
import Control.Applicative

type Config a = [(String, a)]

-- Exported Functions
skipChar :: Char -> Parser ()
skipChar c = skip ((==) c)

parseInteger :: Parser Integer
parseInteger = signed (decimal::Parser Integer)

parseDouble :: Parser Double
parseDouble = double

parseList :: ([a] -> a) -> [Parser a] -> Parser [a]
parseList c parsers = createArrayParser ',' (foldl1 (<|>) (parsers ++ [c <$> (parseList c parsers)]))

parseName :: Parser String
parseName = ((:) <$> letter) <*> many (letter <|> digit)

genListParser :: ([a] -> a) -> [Parser a] -> (Parser a)
genListParser c parsers = liftParser c (parseList c parsers)

genReadConfig :: [Parser a] -> String -> (String, Config a)
genReadConfig parsers content = let cmds         = unzip (splitToCmds content)
                                    row_numbers  = fst cmds 
                                    rows         = snd cmds
                                in
                                    pproConfig $ zip row_numbers (processCommands parsers [] rows)

processCommands :: [Parser a] -> [Either String (String, a)] -> [T.Text] -> [Either String (String, a)]
processCommands _ pre_config [] = pre_config
processCommands parsers pre_config (c:cs)  = let new_preconfig = pre_config ++ [parseOnly ((parseVariable parsers pre_config) <* endOfInput) c] in
                                                 processCommands parsers new_preconfig cs
                                                

liftParser :: (a -> b) -> (Parser a) -> (Parser b)
liftParser c parser = c <$> parser

splitToCmds :: String -> [(Integer, T.Text)]
splitToCmds s = let rows  = map T.pack $ init (splitOneOf ";" (filter (not . (`elem` "\n")) s))
                    counts = [1 .. ((toInteger . length) rows)]
                in
                    zip counts rows
createArrayParser :: Char -> Parser a -> Parser [a]
createArrayParser delim parser =  skipChar '[' *> skipSpace *> ((:) <$> parser)
                              <*> many (skipSpace *> skipChar delim *> skipSpace *> parser)
                              <*  skipChar ']'

createParserFromRead :: String -> (String -> a) -> Parser a
createParserFromRead name f = do
    _ <-  string (T.pack name)
    skipSpace
    skipChar '{'
    content <- takeTill ((==) '}')
    skipSpace
    skipChar '}'
    return $ f (name ++ " " ++ (T.unpack content))


getValueByName :: Config a -> String -> Either String a
getValueByName config name = let result = filter (\(var_name, _) -> var_name == name) config in
                                 if | length result == 0 -> Left  $ "Variable \"" ++ name ++ "\" not defined"
                                    | length result == 1 -> Right $ snd (result !! 0)
                                    | otherwise          -> Left  $ "Mutliple definitions of " ++ name

isErroneous :: (String, Config a) -> Bool
isErroneous (errors, _) = length errors > 0

getErrors :: (String, Config a) -> String
getErrors (errors, _) = errors

getConfig :: (String, Config a) -> (Config a)
getConfig (_, config) = config

-- Locale Functions {{{



isNameFree :: String -> Config a-> Bool
isNameFree name config = length (filter (\(var_name, _) -> var_name == name) config) == 0


parseVariable :: [Parser a] -> [Either String (String, a)] -> Parser (String, a)
parseVariable parsers pre_config = let config = rights pre_config in do
    name <- parseName
    if isNameFree name config then do
        skipSpace
        skipChar '='
        skipSpace
        value <- choice parsers
        return (name, value)
    else
        fail $ "Variable " ++ name ++ " is already defined"


pproConfig :: [(Integer, Either String (String, a))] -> (String, Config a)
pproConfig unprocessed = foldl (bif (++) (++)) ("", []) $ map pproLine unprocessed

pproLine :: (Integer, Either String (String, a)) -> (String, Config a)
pproLine (line_number, Left  result) = (show line_number ++ ": " ++ result, [])
pproLine (_ , Right result) = ("", [result])


bif :: (a->b->c) -> (d->e->f) -> (a, d) -> (b, e) -> (c, f)
bif f g (a, d) (b, e) = (f a b, g d e)


-- }}}


test :: IO ()
test = do
    config_file_handle <- openFile "config.txt" ReadMode
    content <- hGetContents config_file_handle
    result <- return $ genReadConfig [parseInteger] content
    print $ isErroneous result
    print $ result
